"""adding image

Revision ID: 9500947038df
Revises: 45327f75a0ef
Create Date: 2022-02-22 16:41:27.715226

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9500947038df'
down_revision = '45327f75a0ef'
branch_labels = None
depends_on = None


def upgrade():
     op.add_column(
        'posts',
        sa.Column('url',sa.String(200))
    )


def downgrade():
    pass
