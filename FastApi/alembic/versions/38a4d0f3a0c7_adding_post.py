"""adding post

Revision ID: 38a4d0f3a0c7
Revises: 7bc32099ad16
Create Date: 2022-02-22 15:27:29.933837

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '38a4d0f3a0c7'
down_revision = '7bc32099ad16'
branch_labels = None
depends_on = None


def upgrade():
      op.create_table(
        'posts',
        sa.Column('id', sa.Integer, primary_key=True,unique=True),
        sa.Column('title', sa.String(100)),
        sa.Column('body',sa.String(1000)),
        sa.Column('owner_id',sa.Integer),
        sa.Column('is_active',sa.Boolean),
        sa.Column('created_date',sa.DateTime)
    )


def downgrade():
    pass
