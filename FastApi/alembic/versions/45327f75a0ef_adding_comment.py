"""adding comment

Revision ID: 45327f75a0ef
Revises: 38a4d0f3a0c7
Create Date: 2022-02-22 15:28:10.920191

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '45327f75a0ef'
down_revision = '38a4d0f3a0c7'
branch_labels = None
depends_on = None


def upgrade():
     op.create_table(
        'comments',
        sa.Column('id', sa.Integer, primary_key=True,unique=True),
        sa.Column('name', sa.String(100)),
        sa.Column('email', sa.String(1000)),
        sa.Column('body',sa.String(1000)),
        sa.Column('post_id',sa.Integer),
        sa.Column('is_active',sa.Boolean),
        sa.Column('created_date',sa.DateTime)
    )


def downgrade():
    pass
